%dw 2.0
output application/json
var fullName=payload.First_Name ++ " " ++ payload.Last_Name
---
{
	serviceRequest:{
		customerNo: payload.Account_Number as String,
		customerName: fullName,
		business: if(payload.Domain == 'Fire') 1 else if(payload.Domain == 'Security') 2 else if(payload.Domain == 'Havoc') 3 else 0,
		contact:{
			phoneNumber: vars.phoneNumber as Number,
			email: payload.contact_email
		}
	}
}